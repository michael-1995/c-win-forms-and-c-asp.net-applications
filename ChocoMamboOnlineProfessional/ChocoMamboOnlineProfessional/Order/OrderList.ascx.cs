﻿using DBConnection;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ChocoMamboOnlineProfessional.Order
{
    public partial class OrderList : System.Web.UI.UserControl
    {

        #region Variable Declaration

        dbConnection _dbConn = new dbConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString); // connection string of the database this is found in the web.config file
        DataTable _dtb; // Data table variable

        #endregion

        #region Accessors

        /// <summary>
        /// Pre-Condition: Method is called
        /// Post: determine what the access type is for the current user
        /// Description: checks current access right 
        /// </summary>
        /// <param name="pStrFormName"></param>
        /// <param name="phshTemp"></param>
        private void CheckAccessRights(string pStrFormName, Hashtable phshTemp)
        {
            if (phshTemp[pStrFormName].ToString() == "Deny")
            {
                Response.Redirect("/Home/Home.aspx");
            }
            if (phshTemp[pStrFormName].ToString() == "Read")
            {
                //do nothing here because nothing needs to be changed
            }
            if (phshTemp[pStrFormName].ToString() == "Write")
            {
                //do nothing here because nothing needs to be changed
            }
            if (phshTemp[pStrFormName].ToString() == "Admin")
            {
                //do nothing here because nothing needs to be changed
            }
        }

        /// <summary>
        /// Pre-Condition: User enters page 
        /// Post: Grabing the access right from a session variable then call the method and pass 
        /// the current page and current access right
        /// Description: checks the access rights
        /// </summary>
        private void CheckAccessRights()
        {
            Hashtable AccessRights = (Hashtable)Session["AccessRight"];
            if (AccessRights.Contains(("Order")))
            {
                CheckAccessRights("Order", AccessRights);
            }
            if (AccessRights.Contains(("AllPages")))
            {
                CheckAccessRights("AllPages", AccessRights);
            }
            else
            {
                Response.Redirect("/Home/Home.aspx");
            }
        }


        #endregion

        #region Mutators

        /// <summary>
        /// Pre-Condition: Gird view waiting to be filled
        /// Post-Conidition: Binds the grid view data from a data table
        /// Description: opens the connection, grabs data from a query or a table then 
        /// creates a datatable of that query or table then gives the grid view a data source 
        /// of that datatable
        /// </summary>
        private void fillGridView()
        {
            _dtb = _dbConn.GetDataTable(("qryOrders"));
            gvData.DataSource = _dtb.DefaultView;
            gvData.DataBind();
        }

        /// <summary>
        /// Clear the session variable 
        /// </summary>
        private void clearSession()
        {
            Session["OrderPKID"] = "";
        }

        #endregion 

        #region Control Events

        protected void Page_Load(object sender, EventArgs e)
        {
            CheckAccessRights();
            fillGridView();
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            clearSession();
            Response.Redirect("/Order/Order.aspx"); // display a new page
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            if (Session["OrderPKID"].ToString() != "")
            {
                Response.Redirect("/Order/Order.aspx");
            }
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            // get the current row and then give the current id to the session variable 
            GridViewRow row = gvData.SelectedRow;
            Session["OrderPKID"] = row.Cells[1].Text;
        }
        #endregion 
    }
}