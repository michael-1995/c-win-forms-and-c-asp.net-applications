﻿using DBConnection;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ChocoMamboOnlineProfessional.Order
{
    public partial class Order1 : System.Web.UI.UserControl
    {

        #region Variable Declaration

        OrderClass _order = null; // instance of the order class
        long _PKID = 0; // variable used for the current primary key

        #endregion

        #region Accessor

        /// <summary>
        /// display the current record in all the related fields
        /// </summary>
        private void displayRecord()
        {
            cboCustomer.SelectedValue = _order.CustomerID.ToString();
            dtpOrderDate.Text = _order.OrderDate.ToString();
            dtpShipDate.Text = _order.OrderShippingDate.ToString();
            txtShipAddress.Text = _order.OrderShippingAddress;
            txtTotalAmount.Text = _order.OrderTotal.ToString("c2");
            gvOrderLines.DataSource = _order.getOrderLinesTable();
            txtOrderName.Text = _order.OrderName;
        }
        /// <summary>
        /// Pre-Condition: Method gets called
        /// Post-Condition: drop down is passed paremters values of the current drop downlist
        /// datatable, the data memeber and the value field. gives the datasource, data memeber 
        /// data value and then it binds the data
        /// Description: drop down list populates
        /// </summary>
        /// <param name="pdlist"></param>
        /// <param name="pDataTable"></param>
        /// <param name="pstrDataMember"></param>
        /// <param name="pstrDataValueField"></param>
        private void PopulateDropDownList(DropDownList pdlist, DataTable pDataTable, String pstrDataMember, String pstrDataValueField)
        {
            pdlist.DataSource = pDataTable;
            pdlist.DataTextField = pstrDataMember;
            pdlist.DataValueField = pstrDataValueField;
            pdlist.DataBind();
        }
        /// <summary>
        /// Pre-Condition: drop down list are emtpy
        /// Post-Condition: calls the method and passes paremeter values to populate the current 
        /// drop down list
        /// Description: populates multiple drop down list
        /// </summary>
        private void PopulateDropDownList()
        {
            PopulateDropDownList(cboCustomer, _order.getCustomers(), "CustomerName", "CustomerID");
            PopulateDropDownList(cboProduct, _order.getProduct(), "ProductName", "ProductID");
        }
        /// <summary>
        /// Pre-Condition: Method is called
        /// Post: determine what the access type is for the current user
        /// Description: checks current access right 
        /// </summary>
        /// <param name="pStrFormName"></param>
        /// <param name="phshTemp"></param>
        private void CheckAccessRights(string pStrFormName, Hashtable phshTemp)
        {
            if (phshTemp[pStrFormName].ToString() == "Deny")
            {
                Response.Redirect("/Home/Home.aspx");
            }
            if (phshTemp[pStrFormName].ToString() == "Read")
            {
                hideButton(btnSubmit);
                hideButton(btnCalculate);
                hideButton(btnDeleteOrder);
                hideButton(btnInsert);
                disableGridView(gvOrderLines);
            }
            if (phshTemp[pStrFormName].ToString() == "Write")
            {
                //do nothing here because nothing needs to be changed
            }
            if (phshTemp[pStrFormName].ToString() == "Admin")
            {
                //do nothing here because nothing needs to be changed
            }
        }

        /// <summary>
        /// Pre-Condition: User enters page 
        /// Post: Grabing the access right from a session variable then call the method and pass 
        /// the current page and current access right
        /// Description: checks the access rights
        /// </summary>
        private void CheckAccessRights()
        {
            Hashtable AccessRights = (Hashtable)Session["AccessRight"];
            if (AccessRights.Contains(("Order")))
            {
                CheckAccessRights("Order", AccessRights);
            }
            if (AccessRights.Contains(("AllPages")))
            {
                CheckAccessRights("AllPages", AccessRights);
            }
            else
            {
                Response.Redirect("/Home/Home.aspx");
            }
        }

        #endregion

        #region Mutator

        private void assignChildData()
        {
            // assign the values to the variables to be used for calculations
            _order.OrderLineClass.ProductID = long.Parse(cboProduct.SelectedValue.ToString());
            _order.OrderLineClass.OrderLineQty = long.Parse(txtQuantity.Text);
            _order.OrderLineClass.ProductPrice = decimal.Parse(txtPrice.Text.ToString().Substring
                                                          (txtPrice.Text.ToString().IndexOf('$') + 1));
            _order.OrderLineClass.OrderNumber = _order.PKID;
            _order.OrderLineClass.OrderLineSubTotal = lineTotal();
            _order.OrderLineClass.ProductName = cboProduct.SelectedItem.Text;
        }

        /// <summary>
        /// Assign the class properties to the text field values 
        /// </summary>
        private void assignData()
        {
            _order.OrderName = txtOrderName.Text;
            _order.CustomerID = long.Parse(cboCustomer.SelectedValue.ToString());
            _order.OrderDate = dtpOrderDate.Text;
            _order.OrderShippingDate = dtpShipDate.Text;
            _order.OrderShippingAddress = txtShipAddress.Text;
            _order.OrderTotal = decimal.Parse(txtTotalAmount.Text.Substring
                                              (txtTotalAmount.Text.IndexOf('$') + 1));
        }
        /// <summary>
        /// reloads the data source in the grid view
        /// </summary>
        private void reloadGridView()
        {
            gvOrderLines.DataSource = _order.getOrderLinesTable();
            gvOrderLines.DataBind();
        }
        /// <summary>
        /// Pre-Condition: Current OrderLineSubTotal
        /// Post-Condition: Calulates new OrderLineSubTotal
        /// Description: return the Order total
        /// </summary>
        /// <returns> OrderLineSubTotal </returns>
        private decimal orderTotal()
        {
            return Decimal.Parse(_order.getOrderLinesTable().Compute("Sum(OrderLineSubTotal)", "").ToString());
        }
        /// <summary>
        /// Pre-Condition: Total amount hasnt been calculated
        /// Post-Condition: Gets the current total and displays it in the textbox
        /// Description: just displays the order total after being calculated
        /// </summary>
        private void displayOrderTotal()
        {
            if (isGridViewEmpty(gvOrderLines) == true)
                txtTotalAmount.Text = "0";
            else
                txtTotalAmount.Text = orderTotal().ToString("c2");
        }
        /// <summary>
        /// Pre-Condition: Line total eqauls 0
        /// Post-Condition: Calculates the line total by multiplyng the price with the quantity
        /// Description: Calculates the new line total 
        /// </summary>
        /// <returns> price multiply quantity </returns>
        private decimal lineTotal()
        {
            int intQty = int.Parse(txtQuantity.Text);
            decimal decPrice = decimal.Parse(txtPrice.Text.ToString().Substring(txtPrice.Text.ToString().IndexOf('$') + 1));

            return decPrice * intQty;
        }
        /// <summary>
        /// saves the current record
        /// </summary>
        private void save()
        {
            assignData();
            _order.saveData();
            _order.OrderLineClass.saveData();
        }
        /// <summary>
        /// Clear the session variable 
        /// </summary>
        private void clearSession()
        {
            Session["OrderPKID"] = "";
        }
        /// <summary>
        /// Save the session variable 
        /// </summary>
        private void saveSession()
        {
            Session["Order"] = _order;

        }
        /// <summary>
        /// Pre-Condition: Not yet sure if the grid view is emtpy
        /// Post-Condition: Checks if the grid view count is eqauled to 0
        /// Description: To determine if the grid view is empty
        /// </summary>
        /// <param name="pgvTemp"></param>
        /// <returns></returns>
        private Boolean isGridViewEmpty(GridView pgvTemp)
        {
            if (pgvTemp.Rows.Count == 0)
                return true;
            else
                return false;
        }
        /// <summary>
        /// Pre-Condition: Not yet sure if the current text field is empty
        /// Post-Conditio: Checks if the current text field isnt empty
        /// Description: To determine if the text field is empty
        /// </summary>
        /// <param name="ptxtTemp"></param>
        /// <returns></returns>
        private Boolean isCurrentTextFieldNotEmpty(TextBox ptxtTemp)
        {
            if (ptxtTemp.Text != string.Empty)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Pre-Conidition: Button isnt hidden
        /// Post-Conition: hides the button
        /// Description: When this method is called it gets passed a button to the paremeter 
        /// which is a new button object then its visibilty is set to false and then the button 
        /// passed inherits its value
        /// </summary>
        /// <param name="pbtnTemp"></param>
        private void hideButton(Button pbtnTemp)
        {
            pbtnTemp.Visible = false;
        }

        /// <summary>
        /// Pre-Condition: Grid view is enabled
        /// Post-Condition: disables the grid view
        /// Description: When this method is called it gets passed a grid view to the paremeter 
        /// which is a new grid view object then is disables it and then the grid view passed 
        /// inherits its value
        /// </summary>
        /// <param name="pgvTemp"></param>
        private void disableGridView(GridView pgvTemp)
        {
            pgvTemp.Enabled = false;
        }

        #endregion 

        #region Control Events

        protected void Page_Load(object sender, EventArgs e)
        {
            // if the session variable is empty pass the session variable to the global long varible
            if (Session["OrderPKID"].ToString() != "" && long.TryParse(Session["OrderPKID"].ToString(), out _PKID))
            {
                _PKID = long.Parse(Session["OrderPKID"].ToString());
                // if the page isnt a post a back, grab the current record and display it
                if (!Page.IsPostBack)
                {
                    Session["Order"] = _order = new OrderClass(_PKID); // grab the current instance of the order class if post back happen
                    displayRecord();
                    PopulateDropDownList();
                }
                _order = (OrderClass)Session["Order"];
                reloadGridView();
            }
            else
            {
                // if the page isnt a post back, create a new record
                if (!Page.IsPostBack)
                {
                    Session["Order"] = _order = new OrderClass(); // grab the current instance of the order class if post back happen
                    PopulateDropDownList();
                }
                _order = (OrderClass)Session["Order"];
                reloadGridView();
                hideButton(btnDeleteOrder);
            }
            CheckAccessRights(); // check the access rights
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            save(); // save the current record 
            clearSession(); // clear the session
            Response.Redirect("/Order/OrderList.aspx"); // load new page
        }

        protected void btnInsert_Click(object sender, EventArgs e)
        {
            //if the text field being passed isnt empty
            if (isCurrentTextFieldNotEmpty(txtLineTotal))
            {
                assignChildData(); // assign the child data to the class properties 
                _order.OrderLineClass.addNewRecord(); // add a new order line record

                saveSession(); // save the current session
                reloadGridView(); // reload view

                displayOrderTotal(); // display total 
                txtOrderName.Text = cboCustomer.SelectedItem.Text; 
            }
        }

        protected void btnDeleteOrder_Click(object sender, EventArgs e)
        {
            // if the grid view being passed isnt empty
            if (isGridViewEmpty(gvOrderLines) == true)
            {
                //get the current primary key
                _PKID = long.Parse(Session["OrderPKID"].ToString());
                _order.deleteRecord(_PKID); // now delete the order 
                Response.Redirect("/Order/OrderList.aspx");
            }
        }

        protected void cboProduct_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["CurrentProduct"] = cboProduct.SelectedItem.Text;
            DataTable dtbTableData = _order.getProduct();
            // grab all the data rows in the table 
            foreach (DataRow drw in dtbTableData.Rows)
            {
                // if the value in the session matches any of the ProductNames
                // then fill the text field with the current price of that product
                if (Session["CurrentProduct"].Equals(drw["ProductName"].ToString()))
                {
                    txtPrice.Text = drw["Price"].ToString();
                }
            }
        }

        protected void btnCalculate_Click(object sender, EventArgs e)
        {
            // if the passed fields arent empty
            if (isCurrentTextFieldNotEmpty(txtPrice) && isCurrentTextFieldNotEmpty(txtQuantity))
            {
                txtLineTotal.Text = lineTotal().ToString("c2");
            }
        }

        protected void gvOrderLines_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            // if the command is delete orderline then get the current id from the selected row and delete it
            if (e.CommandName == "deleteOrderLine")
            {
                _order.OrderLineClass.deleteOrderLine(long.Parse(gvOrderLines.Rows[int.Parse(e.CommandArgument.ToString())].Cells[1].Text));
                reloadGridView();
                displayOrderTotal();
            }
        }
        #endregion 

    }
}