﻿using DBConnection;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ChocoMamboOnlineProfessional.Employee
{
    public partial class Employee : System.Web.UI.UserControl
    {

        #region Variable Declaration

        EmployeeClass _employee = null; // instance of the employee class
        long _PKID = 0; // variable used for the current primary key

        #endregion 

        #region Accessor
        /// <summary>
        /// display the current record in all the related fields
        /// </summary>
        private void displayRecord()
        {
            txtFirstName.Text = _employee.FirstName;
            txtLastName.Text = _employee.LastName;
            txtPhone.Text = _employee.Phone;
            txtAddress.Text = _employee.AddressLine1;
            txtPostCode.Text = _employee.Postcode;
            txtSuburb.Text = _employee.Suburb;
            txtState.Text = _employee.State;
            cboDepartment.Text = _employee.Department;
            txtSalary.Text = _employee.Salary;
            txtUsername.Text = _employee.UserName;
            txtPassword.Text = _employee.UserPassword;
        }

        /// <summary>
        /// Pre-Condition: Method is called
        /// Post: determine what the access type is for the current user
        /// Description: checks current access right 
        /// </summary>
        /// <param name="pStrFormName"></param>
        /// <param name="phshTemp"></param>
        private void CheckAccessRights(string pStrFormName, Hashtable phshTemp)
        {
            if (phshTemp[pStrFormName].ToString() == "Deny")
            {
                Response.Redirect("/Home/Home.aspx");
            }
            if (phshTemp[pStrFormName].ToString() == "Read")
            {
                hideButton(btnSubmit);
            }
            if (phshTemp[pStrFormName].ToString() == "Write")
            {
                //do nothing here because nothing needs to be changed
            }
            if (phshTemp[pStrFormName].ToString() == "Admin")
            {
                //do nothing here because nothing needs to be changed
            }
        }

        /// <summary>
        /// Pre-Condition: User enters page 
        /// Post: Grabing the access right from a session variable then call the method and pass 
        /// the current page and current access right
        /// Description: checks the access rights
        /// </summary>
        private void CheckAccessRights()
        {
            Hashtable AccessRights = (Hashtable)Session["AccessRight"];
            if (AccessRights.Contains(("Employee")))
            {
                CheckAccessRights("Employee", AccessRights);
            }
            if (AccessRights.Contains(("AllPages")))
            {
                CheckAccessRights("AllPages", AccessRights);
            }
            else
            {
                Response.Redirect("/Home/Home.aspx");
            }
        }


        #endregion 

        #region Mutator
        /// <summary>
        /// assign the related fields to the class
        /// </summary>
        private void AssignData()
        {
            _employee.FirstName = txtFirstName.Text;
            _employee.LastName = txtLastName.Text;
            _employee.Phone = txtPhone.Text;
            _employee.AddressLine1 = txtAddress.Text;
            _employee.Postcode = txtPostCode.Text;
            _employee.Suburb = txtSuburb.Text;
            _employee.State = txtState.Text;
            _employee.Department = cboDepartment.Text;
            _employee.Salary = txtSalary.Text;
            _employee.UserName = txtUsername.Text;
            _employee.UserPassword = txtPassword.Text;
        }

        /// <summary>
        /// Pre-Conidition: Button isnt hidden
        /// Post-Conition: hides the button
        /// Description: When this method is called it gets passed a button to the paremeter 
        /// which is a new button object that buttons visibilty is set to false
        /// </summary>
        /// <param name="pbtnTemp"></param>
        private void hideButton(Button pbtnTemp)
        {
            pbtnTemp.Visible = false;
        }

        /// <summary>
        /// Clear the session variable 
        /// </summary>
        private void clearSession()
        {
            Session["EmployeePKID"] = "";
        }

        #endregion 

        #region Control Events

        protected void Page_Load(object sender, EventArgs e)
        {
            // if the session variable is empty pass the session variable to the global long varible
            if (Session["EmployeePKID"].ToString() != "" && long.TryParse(Session["EmployeePKID"].ToString(), out _PKID))
            {
                _PKID = long.Parse(Session["EmployeePKID"].ToString());
                // if the page isnt a post a back, grab the current record and display it
                if (!Page.IsPostBack)
                {
                    Session["Employee"] = _employee = new EmployeeClass(_PKID);
                    displayRecord();
                }
                _employee = (EmployeeClass)Session["Employee"]; // grab the current instance of the employee class if post back happen
            }
            else
            {
                // if the page isnt a post back, create a new record
                if (!Page.IsPostBack)
                {
                    Session["Employee"] = _employee = new EmployeeClass();
                }
                _employee = (EmployeeClass)Session["Employee"]; // grab the current instance of the employee class if post back happen
            }
            CheckAccessRights(); // check the access rights
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            AssignData(); // assign the data
            _employee.saveData(); // save the current record
            clearSession(); // clear the current session variable
            Response.Redirect("/Employee/EmployeeList.aspx"); // load the page
        }
        #endregion 

    }
}