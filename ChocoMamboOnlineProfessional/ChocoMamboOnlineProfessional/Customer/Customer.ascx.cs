﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ChocoMamboOnlineProfessional.Customer
{
    public partial class Customer : System.Web.UI.UserControl
    {

        #region Variable Declaration

        CustomerClass _customer = null; // instance of the customer class
        long _PKID = 0; // variable used for the current primary key

        #endregion

        #region Accessor

        /// <summary>
        /// display the current record in all the related fields
        /// </summary>
        private void displayRecord()
        {
            cboEmployee.SelectedValue = _customer.EmployeeID.ToString();
            txtCustomerName.Text = _customer.CustomerName;
            txtPhone.Text = _customer.Phone;
            txtAddress.Text = _customer.Address;
            txtPostCode.Text = _customer.Postcode;
            txtSuburb.Text = _customer.Suburb;
            txtState.Text = _customer.State;
        }
        /// <summary>
        /// Pre-Condition: Combo box is empty
        /// Post-Condition: give the data source of the current combo box from the table or query
        /// Value the Employee ID and Show the First Name in the combo box then bind the data
        /// Description: use this method only because there is one combo box to populate 
        /// </summary>
        private void populateEmployeeComboBox()
        {
            cboEmployee.DataSource = _customer.getEmployees();
            cboEmployee.DataValueField = "EmployeeID";
            cboEmployee.DataTextField = "FirstName";
            cboEmployee.DataBind();
        }

        /// <summary>
        /// Pre-Condition: Method is called
        /// Post: determine what the access type is for the current user
        /// Description: checks current access right 
        /// </summary>
        /// <param name="pStrFormName"></param>
        /// <param name="phshTemp"></param>
        private void CheckAccessRights(string pStrFormName, Hashtable phshTemp)
        {
            if (phshTemp[pStrFormName].ToString() == "Deny")
            {
                Response.Redirect("/Home/Home.aspx");
            }
            if (phshTemp[pStrFormName].ToString() == "Read")
            {
                hideButton(btnSubmit);
            }
            if (phshTemp[pStrFormName].ToString() == "Write")
            {
                //do nothing here because nothing needs to be changed
            }
            if (phshTemp[pStrFormName].ToString() == "Admin")
            {
                //do nothing here because nothing needs to be changed
            }
        }
        /// <summary>
        /// Pre-Condition: User enters page 
        /// Post: Grabing the access right from a session variable then call the method and pass 
        /// the current page and current access right
        /// Description: checks the access rights
        /// </summary>
        private void CheckAccessRights()
        {
            Hashtable AccessRights = (Hashtable)Session["AccessRight"];
            if (AccessRights.Contains(("Customer")))
            {
                CheckAccessRights("Customer", AccessRights);
            }
            if (AccessRights.Contains(("AllPages")))
            {
                CheckAccessRights("AllPages", AccessRights);
            }
            else
            {
                Response.Redirect("/Home/Home.aspx");
            }
        }


        #endregion

        #region Mutator

        /// <summary>
        /// assign the related fields to the class
        /// </summary>
        private void AssignData()
        {
            _customer.EmployeeID = long.Parse(cboEmployee.SelectedValue.ToString());
            _customer.CustomerName = txtCustomerName.Text;
            _customer.Phone = txtPhone.Text;
            _customer.Address = txtAddress.Text;
            _customer.Postcode = txtPostCode.Text;
            _customer.Suburb = txtSuburb.Text;
            _customer.State = txtState.Text;
        }
        /// <summary>
        /// Pre-Conidition: Button isnt hidden
        /// Post-Conition: hides the button
        /// Description: When this method is called it gets passed a button to the paremeter 
        /// which is a new button object that buttons visibilty is set to false
        /// </summary>
        /// <param name="pbtnTemp"></param>
        private void hideButton(Button pbtnTemp)
        {
            pbtnTemp.Visible = false;
        }
        /// <summary>
        /// Clear the session variable 
        /// </summary>
        private void clearSession()
        {
            Session["CustomerPKID"] = "";
        }

        #endregion 

        #region Control Events

        protected void Page_Load(object sender, EventArgs e)
        {
            // if the session variable is empty pass the session variable to the global long varible
            if (Session["CustomerPKID"].ToString() != "" && long.TryParse(Session["CustomerPKID"].ToString(), out _PKID))
            {
                _PKID = long.Parse(Session["CustomerPKID"].ToString());
                // if the page isnt a post a back, grab the current record and display it
                if (!Page.IsPostBack)
                {
                    Session["Customer"] = _customer = new CustomerClass(_PKID);
                    displayRecord();
                    populateEmployeeComboBox();
                }
                _customer = (CustomerClass)Session["Customer"]; // grab the current instance of the customer class if post back happen
            }
            else
            {
                // if the page isnt a post back, create a new record
                if (!Page.IsPostBack)
                {
                    Session["Customer"] = _customer = new CustomerClass();
                    populateEmployeeComboBox();
                }
                _customer = (CustomerClass)Session["Customer"]; // grab the current instance of the customer class if post back happen
            }
            CheckAccessRights(); // check the access rights
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            AssignData(); // assign the data
            _customer.saveData(); // save the current record
            clearSession(); // clear the current session variable
            Response.Redirect("/Customer/CustomerList.aspx"); // load the page
        }
        #endregion 

    }
}