﻿using DBConnection;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ChocoMamboOnlineProfessional.Purchase
{
    public partial class Purchase1 : System.Web.UI.UserControl
    {

        #region Variable Declaration

        PurchaseClass _purchase = null; // instance of the purchase class
        long _PKID = 0; // variable used for the current primary key

        #endregion

        #region Accessor

        /// <summary>
        /// display the current record in all the related fields
        /// </summary>
        private void displayRecord()
        {
            cboSupplier.SelectedValue = _purchase.SupplierID.ToString();
            cboBranch.SelectedValue = _purchase.BranchID.ToString();
            dtpDatePurchased.Text = _purchase.DatePurchased;
            txtTotalAmount.Text = _purchase.PurchaseTotal.ToString("c2");
            gvPurchaseLines.DataSource = _purchase.getPurchaseLinesTable();
            txtPurchaseCode.Text = _purchase.PurchaseCode;
        }

        /// <summary>
        /// Pre-Condition: Method gets called
        /// Post-Condition: drop down is passed paremters values of the current drop downlist
        /// datatable, the data memeber and the value field. gives the datasource, data memeber 
        /// data value and then it binds the data
        /// Description: drop down list populates
        /// </summary>
        /// <param name="pdlist"></param>
        /// <param name="pDataTable"></param>
        /// <param name="pstrDataMember"></param>
        /// <param name="pstrDataValueField"></param>
        private void PopulateDropDownList(DropDownList pdlist, DataTable pDataTable, String pstrDataMember, String pstrDataValueField)
        {
            pdlist.DataSource = pDataTable;
            pdlist.DataTextField = pstrDataMember;
            pdlist.DataValueField = pstrDataValueField;
            pdlist.DataBind();
        }

        /// <summary>
        /// Pre-Condition: drop down list are emtpy
        /// Post-Condition: calls the method and passes paremeter values to populate the current 
        /// drop down list
        /// Description: populates multiple drop down list
        /// </summary>
        private void PopulateDropDownList()
        {
            PopulateDropDownList(cboSupplier, _purchase.getSupplier(), "SupplierName", "SupplierID");
            PopulateDropDownList(cboBranch, _purchase.getBranch(), "BranchOffice", "BranchID");
            PopulateDropDownList(cboRawIngredients, _purchase.getRawIngredients(), "IngredientName", "RawIngredientsID");
        }

        /// <summary>
        /// Pre-Condition: drop down list are emtpy
        /// Post-Condition: calls the method and passes paremeter values to populate the current 
        /// drop down list
        /// Description: populates multiple drop down list
        /// </summary>
        private void CheckAccessRights(string pStrFormName, Hashtable phshTemp)
        {
            if (phshTemp[pStrFormName].ToString() == "Deny")
            {
                Response.Redirect("/Home/Home.aspx");
            }
            if (phshTemp[pStrFormName].ToString() == "Read")
            {
                hideButton(btnSubmit);
                hideButton(btnCalculate);
                hideButton(btnDeletePurchase);
                hideButton(btnInsert);
                disableGridView(gvPurchaseLines);
            }
            if (phshTemp[pStrFormName].ToString() == "Write")
            {
                //do nothing here because nothing needs to be changed
            }
            if (phshTemp[pStrFormName].ToString() == "Admin")
            {
                //do nothing here because nothing needs to be changed
            }
        }

        /// <summary>
        /// Pre-Condition: User enters page 
        /// Post: Grabing the access right from a session variable then call the method and pass 
        /// the current page and current access right
        /// Description: checks the access rights
        /// </summary>
        private void CheckAccessRights()
        {
            Hashtable AccessRights = (Hashtable)Session["AccessRight"];
            if (AccessRights.Contains(("Purchase")))
            {
                CheckAccessRights("Purchase", AccessRights);
            }
            if (AccessRights.Contains(("AllPages")))
            {
                CheckAccessRights("AllPages", AccessRights);
            }
            else
            {
                Response.Redirect("/Home/Home.aspx");
            }
        }

        #endregion

        #region Mutator

        private void assignChildData()
        {
            // assign the values to the variables to be used for calculations
            _purchase.PurchaseLineClass.RawIngredientsID = long.Parse(cboRawIngredients.SelectedValue.ToString());
            _purchase.PurchaseLineClass.Price = decimal.Parse(txtPrice.Text.ToString().Substring
                                                          (txtPrice.Text.ToString().IndexOf('$') + 1));
            _purchase.PurchaseLineClass.SupplierLineQty = long.Parse(txtQuantity.Text);
            _purchase.PurchaseLineClass.SupplierLineSubTotal = lineTotal();
            _purchase.PurchaseLineClass.PurchaseID = _purchase.PKID;
            _purchase.PurchaseLineClass.IngredientName = cboRawIngredients.SelectedItem.Text;
        }

        /// <summary>
        /// Assign the class properties to the text field values
        /// </summary>
        private void assignData()
        {
            _purchase.PurchaseCode = txtPurchaseCode.Text;
            _purchase.BranchID = long.Parse(cboBranch.SelectedValue.ToString());
            _purchase.DatePurchased = dtpDatePurchased.Text;
            _purchase.SupplierID = long.Parse(cboSupplier.SelectedValue.ToString());
            _purchase.PurchaseTotal = decimal.Parse(txtTotalAmount.Text.ToString().Substring
                                              (txtTotalAmount.Text.ToString().IndexOf('$') + 1));
        }

        /// <summary>
        /// reloads the data source in the grid view
        /// </summary>
        private void reloadGridView()
        {
            gvPurchaseLines.DataSource = _purchase.getPurchaseLinesTable();
            gvPurchaseLines.DataBind();
        }

        /// <summary>
        /// Pre-Condition: Current OrderLineSubTotal
        /// Post-Condition: Calulates new OrderLineSubTotal
        /// Description: return the Order total
        /// </summary>
        /// <returns> OrderLineSubTotal </returns>
        private decimal orderTotal()
        {
            return decimal.Parse(_purchase.getPurchaseLinesTable().Compute
                                                      ("Sum(SupplierLineSubTotal)", "").ToString());
        }

        /// <summary>
        /// Pre-Condition: Total amount hasnt been calculated
        /// Post-Condition: Gets the current total and displays it in the textbox
        /// Description: just displays the order total after being calculated
        /// </summary>
        private void displayOrderTotal()
        {
            if (isGridViewEmpty(gvPurchaseLines) == true)
                txtTotalAmount.Text = "0";
            else
                txtTotalAmount.Text = orderTotal().ToString("c2");
        }

        /// <summary>
        /// Pre-Condition: Line total eqauls 0
        /// Post-Condition: Calculates the line total by multiplyng the price with the quantity
        /// Description: Calculates the new line total 
        /// </summary>
        /// <returns> price multiply quantity </returns>
        private decimal lineTotal()
        {
            int intQty = int.Parse(txtQuantity.Text);
            decimal decPrice = decimal.Parse(txtPrice.Text.ToString().Substring(txtPrice.Text.ToString().IndexOf('$') + 1));

            return decPrice * intQty;
        }

        /// <summary>
        /// saves the current record
        /// </summary>
        private void save()
        {
            assignData();
            _purchase.saveData();
            _purchase.PurchaseLineClass.saveData();
        }

        /// <summary>
        /// Clear the session variable 
        /// </summary>
        private void clearSession()
        {
            Session["PurchasePKID"] = "";
        }

        /// <summary>
        /// Save the session variable 
        /// </summary>
        private void saveSession()
        {
            Session["Purchase"] = _purchase;

        }

        /// <summary>
        /// Pre-Condition: Not yet sure if the grid view is emtpy
        /// Post-Condition: Checks if the grid view count is eqauled to 0
        /// Description: To determine if the grid view is empty
        /// </summary>
        /// <param name="pgvTemp"></param>
        /// <returns></returns>
        private Boolean isGridViewEmpty(GridView pgvTemp)
        {
            if (pgvTemp.Rows.Count == 0)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Pre-Condition: Not yet sure if the current text field is empty
        /// Post-Conditio: Checks if the current text field isnt empty
        /// Description: To determine if the text field is empty
        /// </summary>
        /// <param name="ptxtTemp"></param>
        /// <returns></returns>
        private Boolean isCurrentTextFieldNotEmpty(TextBox ptxtTemp)
        {
            if (ptxtTemp.Text != string.Empty)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Pre-Conidition: Button isnt hidden
        /// Post-Conition: hides the button
        /// Description: When this method is called it gets passed a button to the paremeter 
        /// which is a new button object then its visibilty is set to false and then the button 
        /// passed inherits its value
        /// </summary>
        /// <param name="pbtnTemp"></param>
        private void hideButton(Button pbtnTemp)
        {
            pbtnTemp.Visible = false;
        }

        /// <summary>
        /// Pre-Condition: Grid view is enabled
        /// Post-Condition: disables the grid view
        /// Description: When this method is called it gets passed a grid view to the paremeter 
        /// which is a new grid view object then is disables it and then the grid view passed 
        /// inherits its value
        /// </summary>
        /// <param name="pgvTemp"></param>
        private void disableGridView(GridView pgvTemp)
        {
            pgvTemp.Enabled = false;
        }

        #endregion 

        #region Control Event

        protected void Page_Load(object sender, EventArgs e)
        {
            // if the session variable is empty pass the session variable to the global long varible
            if (Session["PurchasePKID"].ToString() != "" && long.TryParse(Session["PurchasePKID"].ToString(), out _PKID))
            {
                _PKID = long.Parse(Session["PurchasePKID"].ToString());
                // if the page isnt a post a back, grab the current record and display it
                if (!Page.IsPostBack)
                {
                    Session["Purchase"] = _purchase = new PurchaseClass(_PKID); 
                    displayRecord();
                    PopulateDropDownList();
                }
                _purchase = (PurchaseClass)Session["Purchase"]; // grab the current instance of the pruchase class if post back happen
                reloadGridView(); 
            }
            else
            {
                // if the page isnt a post back, create a new record
                if (!Page.IsPostBack)
                {
                    Session["Purchase"] = _purchase = new PurchaseClass();
                    PopulateDropDownList();
                }
                _purchase = (PurchaseClass)Session["Purchase"]; // grab the current instance of the pruchase class if post back happen
                reloadGridView();
                hideButton(btnDeletePurchase);
            }
            CheckAccessRights(); // check the access rights
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            save(); // save the current record 
            clearSession(); // clear the session
            Response.Redirect("/Purchase/PurchaseList.aspx"); // load new page
        }

        protected void btnCalculate_Click(object sender, EventArgs e)
        {
            // if the passed fields arent empty
            if (isCurrentTextFieldNotEmpty(txtPrice) && isCurrentTextFieldNotEmpty(txtQuantity))
            {
                txtLineTotal.Text = lineTotal().ToString("c2");
            }
        }

        protected void btnInsert_Click(object sender, EventArgs e)
        {
            //if the text field being passed isnt empty
            if (isCurrentTextFieldNotEmpty(txtLineTotal))
            {
                assignChildData(); // assign the child data to the class properties 
                _purchase.PurchaseLineClass.addNewRecord(); // add a new order line record

                saveSession(); // save the current session
                reloadGridView(); // reload view

                displayOrderTotal(); // display total 
                txtPurchaseCode.Text = cboBranch.SelectedItem.Text;
            }
        }

        protected void btnDeleteOrder_Click(object sender, EventArgs e)
        {
            // if the grid view being passed isnt empty
            if (isGridViewEmpty(gvPurchaseLines) == true)
            {
                //get the current primary key
                _PKID = long.Parse(Session["PurchasePKID"].ToString());
                _purchase.deleteRecord(_PKID); // now delete the order 
                Response.Redirect("/Purchase/PurchaseList.aspx");
            }
        }

        protected void cboRawIngredients_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["CurrentIngredient"] = cboRawIngredients.SelectedItem.Text;
            DataTable dtbTableData = _purchase.getRawIngredients();
            // grab all the data rows in the table 
            foreach (DataRow drw in dtbTableData.Rows)
            {
                // if the value in the session matches any of the ProductNames
                // then fill the text field with the current price of that product
                if (Session["CurrentIngredient"].Equals(drw["IngredientName"].ToString()))
                {
                    txtPrice.Text = drw["Price"].ToString();
                }
            }
        }

        protected void gvPurchaseLines_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            // if the command is delete pruchaseline then get the current id from the selected row and delete it
            if (e.CommandName == "deletePurchaseLine")
            {
                _purchase.PurchaseLineClass.deletePurchaseLine(long.Parse(gvPurchaseLines.Rows[int.Parse(e.CommandArgument.ToString())].Cells[1].Text));
                reloadGridView();
                displayOrderTotal();
            }
        }
        #endregion

    }
}