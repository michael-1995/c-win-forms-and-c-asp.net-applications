﻿using DBConnection;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ChocoMamboOnlineProfessional
{
    public partial class UserLogin : System.Web.UI.UserControl
    {

        #region Variable Declaration 

        public long _lngPKID = 0; // variable used for the current primary key
        dbConnection _dbConn = new dbConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString); // connection string of the database this is found in the web.config file

        #endregion 

        #region Accessor
        /// <summary>
        /// method for the hash table 
        /// read from the employee forms table to determine the access rights by grabbing the
        /// employee ID and to see what form they can use by selecting the form ID
        /// and return the value for other methods to use it
        /// </summary>
        /// <returns> HashTable </returns>
        private Hashtable getAccessRightsHashTable()
        {
            Hashtable hshAccessRights = new Hashtable();
            long lngFormID = 0;
            DataTable dtbForms = _dbConn.GetDataTable("tblForms");
            DataTable dtbEmployeeForms = _dbConn.GetDataTable("SELECT * FROM tblEmployeeForms " + "WHERE EmployeeID = " + _lngPKID, "tblEmployeeForms");
            foreach (DataRow drwForms in dtbForms.Rows)
            {
                foreach (DataRow drwEmployeeForms in dtbEmployeeForms.Rows)
                {
                    lngFormID = long.Parse(drwForms["PageID"].ToString());

                    if (lngFormID == long.Parse(drwEmployeeForms["PageID"].ToString()) &&
                        _lngPKID == long.Parse(drwEmployeeForms["EmployeeID"].ToString()))
                    {
                        hshAccessRights.Add(drwForms["PageName"].ToString(), drwEmployeeForms["AccessType"].ToString());
                    }
                }
            }
            return hshAccessRights;
        }
        /// <summary>
        /// allow login to see if the user can login with the right 
        /// login details by reading from employee table 
        /// also if the user is active aswell as having the login in details they can succesfully
        /// login
        /// and return the value of the boolean
        /// </summary>
        /// <returns> blnReturnValue </returns>
        private bool allowLogin()
        {
            bool blnReturnValue = false;
            DataTable dtbStaff = _dbConn.GetDataTable("tblEmployees");
            foreach (DataRow drw in dtbStaff.Rows)
            {
                if (txtUsername.Text.Equals(drw["UserName"].ToString()) &&
                    txtPassword.Text.Equals(drw["UserPassword"].ToString()))
                {
                    _lngPKID = long.Parse(drw["EmployeeID"].ToString());
                    blnReturnValue = true;
                    break;
                }
            }
            return blnReturnValue;
        }

        #endregion 

        #region Mutator


        #endregion 

        #region Control Events

        protected void Page_Load(object sender, EventArgs e)
        {
            Session.Contents.Clear(); // clear all the session variables
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            // if allow login is true
            if (allowLogin())
            {
                Session["AccessRight"] = getAccessRightsHashTable(); // create a session variable for the access rights and pass the method to it 
                Response.Redirect("/Home/Home.aspx"); // load the page
            }
        }

        #endregion 

    }
}