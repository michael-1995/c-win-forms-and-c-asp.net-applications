﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ChocoMamboOnlineProfessional.Branch
{
    public partial class Branch1 : System.Web.UI.UserControl
    {

        #region Variable Declaration

        BranchClass _branch = null; // instance of the branch class
        long _PKID = 0; // variable used for the current primary key

        #endregion

        #region Accessor

        /// <summary>
        /// display the current record in all the related fields
        /// </summary>
        private void displayRecord()
        {
            txtBranchOffice.Text = _branch.BranchOffice;  
        }
        /// <summary>
        /// Pre-Condition: Method is called
        /// Post: determine what the access type is for the current user
        /// Description: checks current access right 
        /// </summary>
        /// <param name="pStrFormName"></param>
        /// <param name="phshTemp"></param>
        private void CheckAccessRights(string pStrFormName, Hashtable phshTemp)
        {
            if (phshTemp[pStrFormName].ToString() == "Deny")
            {
                Response.Redirect("/Home/Home.aspx");
            }
            if (phshTemp[pStrFormName].ToString() == "Read")
            {
                hideButton(btnSubmit);
            }
            if (phshTemp[pStrFormName].ToString() == "Write")
            {
                //do nothing here because nothing needs to be changed
            }
            if (phshTemp[pStrFormName].ToString() == "Admin")
            {
                //do nothing here because nothing needs to be changed
            }
        }
        /// <summary>
        /// Pre-Condition: User enters page 
        /// Post: Grabing the access right from a session variable then call the method and pass 
        /// the current page and current access right
        /// Description: checks the access rights
        /// </summary>
        private void CheckAccessRights()
        {
            Hashtable AccessRights = (Hashtable)Session["AccessRight"];
            if (AccessRights.Contains(("Branch")))
            {
                CheckAccessRights("Branch", AccessRights);
            }
            if (AccessRights.Contains(("AllPages")))
            {
                CheckAccessRights("AllPages", AccessRights);
            }
            else
            {
                Response.Redirect("/Home/Home.aspx");
            }
        }

        #endregion

        #region Mutator

        /// <summary>
        /// assign the related fields to the class
        /// </summary>
        private void AssignData()
        {
            _branch.BranchOffice = txtBranchOffice.Text;
        }

        /// <summary>
        /// Pre-Conidition: Button isnt hidden
        /// Post-Conition: hides the button
        /// Description: When this method is called it gets passed a button to the paremeter 
        /// which is a new button object that buttons visibilty is set to false
        /// </summary>
        /// <param name="pbtnTemp"></param>
        private void hideButton(Button pbtnTemp)
        {
            pbtnTemp.Visible = false;
        }

        /// <summary>
        /// Clear the session variable 
        /// </summary>
        private void clearSession()
        {
            Session["BranchPKID"] = "";
        }

        #endregion 

        #region Control Events

        protected void Page_Load(object sender, EventArgs e)
        {
            // if the session variable is empty pass the session variable to the global long varible
            if (Session["BranchPKID"].ToString() != "" && long.TryParse(Session["BranchPKID"].ToString(), out _PKID))
            {
                _PKID = long.Parse(Session["BranchPKID"].ToString());
                // if the page isnt a post a back, grab the current record and display it
                if (!Page.IsPostBack)
                {
                    Session["Branch"] = _branch = new BranchClass(_PKID);
                    displayRecord();
                }
                _branch = (BranchClass)Session["Branch"]; // grab the current instance of the branch class if post back happen
            }
            else
            {
                // if the page isnt a post back, create a new record
                if (!Page.IsPostBack)
                {
                    Session["Branch"] = _branch = new BranchClass();
                }
                _branch = (BranchClass)Session["Branch"]; // grab the current instance of the branch class if post back happen
            }
            CheckAccessRights(); // check the access rights
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            AssignData(); // assign the data
            _branch.saveData(); // save the current record
            clearSession(); // clear the current session variable
            Response.Redirect("/Branch/BranchList.aspx"); // load the page
        }
        #endregion 
    }
}