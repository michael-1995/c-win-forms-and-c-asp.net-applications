﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Help.ascx.cs" Inherits="ChocoMamboOnlineProfessional.Help.Help" %>
<style type="text/css">
    .auto-style1 {
        width: 100%;
    }
    .auto-style2 {
        height: 24px;
    }
    .auto-style3 {
        text-align: left;
    }
</style>

<table class="auto-style1">
    <tr>
        <td class="auto-style3">
            <h1><strong>
                <asp:HyperLink ID="hplEmployeeListPage" runat="server" NavigateUrl="~/Employee/EmployeeList.aspx">Employee</asp:HyperLink>
                </strong></h1>
        </td>
        <td>
            <asp:Button ID="btnHome" runat="server" OnClick="btnHome_Click" Text="Home" Width="94px" />
        </td>
    </tr>
    <tr>
        <td class="auto-style3">&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <asp:Image ID="Image1" runat="server" Height="299px" ImageUrl="~/Images/5.PNG" Width="623px" />
        </td>
        <td>
            <h2>Step 1.</h2>
            <p>
                &nbsp;<br />
                <br />
                If your access level allows you to enter the Employee Page, you should see a list of Employee&#39;s.</p>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Image ID="Image2" runat="server" Height="299px" ImageUrl="~/Images/6.PNG" Width="623px" />
        </td>
        <td>
            <h2>Step 2.<br />
            </h2>
            <p>
                <br />
                If you dont have all the buttons that means your access levels dont give you all the features.<br />
                <br />
                Press the select link next to an Employee before you press the Edit or Delete buttons</p>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style2">
            <asp:Image ID="Image3" runat="server" Height="299px" ImageUrl="~/Images/7.PNG" Width="623px" />
        </td>
        <td class="auto-style2">
            <h2>Step 3.<br />
            </h2>
            <p>
                <br />
                After pressing the Edit button you will see the current Employee&#39;s details<br />
                <br />
                <br />
                If you pressed the Delete then, Then you deleted the record</p>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <asp:Image ID="Image4" runat="server" Height="299px" ImageUrl="~/Images/8.PNG" Width="623px" />
        </td>
        <td>
            <h2>Step 4.
                <br />
            </h2>
            <p>
                <br />
                If you pressed the new button you will see this page. Once you fill each field correctly, you then need to press the submit button.<br />
                <br />
                If successful it will take you back to the Employee List Page<br />
            </p>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <h1>
                <asp:HyperLink ID="hplCustomerListPage" runat="server" NavigateUrl="~/Customer/CustomerList.aspx">Customer</asp:HyperLink>
            </h1>
        </td>
        <td>
            <asp:Button ID="btnHome0" runat="server" OnClick="btnHome0_Click" Text="Home" Width="94px" />
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <asp:Image ID="Image5" runat="server" Height="299px" ImageUrl="~/Images/9.PNG" Width="623px" />
        </td>
        <td>
            <h2>Step 1.
                <br />
            </h2>
            <p>
                <br />
                This is the Customer List Page.<br />
                <br />
                Select a Customer</p>
            <p>
                Or create a new Customer Record press (new)</p>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Image ID="Image6" runat="server" Height="299px" ImageUrl="~/Images/10.PNG" Width="623px" />
        </td>
        <td>
            <h2>Step 2.<br />
            </h2>
            <p>
                Select a Customer</p>
            <p>
                <br />
                Delete a Customer Record press (Delete)</p>
            <p>
                Or Edit a customer press (Edit)</p>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <asp:Image ID="Image7" runat="server" Height="299px" ImageUrl="~/Images/11.PNG" Width="623px" />
        </td>
        <td>
            <h2>Step 3.<br />
            </h2>
            <p>
                <br />
                You can edit this current Customer<br />
                <br />
                Then Submit it</p>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <asp:Image ID="Image8" runat="server" Height="299px" ImageUrl="~/Images/12.PNG" Width="623px" />
        </td>
        <td>
            <h2>Step 4.
                <br />
            </h2>
            <p>
                <br />
                If you have selected the New button.
                <br />
                <br />
                You will be able to create a new Customer Record then Submit it</p>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <h1>
                <asp:HyperLink ID="hplOrderListPage" runat="server" NavigateUrl="~/Order/OrderList.aspx">Order</asp:HyperLink>
            </h1>
        </td>
        <td>
            <asp:Button ID="btnHome1" runat="server" OnClick="btnHome1_Click" Text="Home" Width="94px" />
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <asp:Image ID="Image9" runat="server" Height="299px" ImageUrl="~/Images/13.PNG" Width="623px" />
        </td>
        <td>
            <h2>Step 1.
                <br />
            </h2>
            <p>
                <br />
                This is the Order List Page.<br />
                <br />
                Select a Order</p>
            <p>
                Or create a new Order Record press (new)</p>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <asp:Image ID="Image10" runat="server" Height="299px" ImageUrl="~/Images/14.PNG" Width="623px" />
        </td>
        <td>
            <h2>Step 2.<br />
            </h2>
            <p>
                Viewing or editing a Order</p>
            <p>
                Before deleting the Order you must Delete all the orderlines.</p>
            <p>
                Submit once all fields are filled</p>
            <p>
                Insert the product you wish to Order
            </p>
            <p>
                Or calculate the line total&nbsp;
            </p>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <asp:Image ID="Image11" runat="server" Height="299px" ImageUrl="~/Images/15.PNG" Width="623px" />
        </td>
        <td>
            <h2>Step 3.
                <br />
            </h2>
            <p>
                <br />
                If creating a new Order Record<br />
                <br />
                Fill the fields that arent grey</p>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <asp:Image ID="Image12" runat="server" Height="299px" ImageUrl="~/Images/16.PNG" Width="623px" />
        </td>
        <td>
            <h2>Step 4.
                <br />
            </h2>
            <p>
                <br />
                Once you have selected a product and given the Quantity you want to buy
                <br />
                <br />
                Press (Calculate)</p>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <asp:Image ID="Image13" runat="server" Height="299px" ImageUrl="~/Images/17.PNG" Width="623px" />
        </td>
        <td>
            <h2>Step 5.<br />
            </h2>
            <p>
                <br />
                Press (Insert)</p>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <asp:Image ID="Image14" runat="server" Height="299px" ImageUrl="~/Images/18.PNG" Width="623px" />
        </td>
        <td>
            <h2>Step 6.
                <br />
            </h2>
            <p>
                <br />
                Now you can Submit the Order<br />
                <br />
                Delete the Order Line<br />
                <br />
                Then Delete the Order<br />
                <br />
                Or insert and calculate a new Product into the Order</p>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <asp:Image ID="Image15" runat="server" Height="299px" ImageUrl="~/Images/19.PNG" Width="623px" />
        </td>
        <td>
            <h2>Step 7.<br />
            </h2>
            <p>
                <br />
                After a sucessful Submit<br />
                <br />
                You will be redirected to the Order List Page</p>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <h1>
                <asp:HyperLink ID="phlProductListPage" runat="server" NavigateUrl="~/Product/ProductList.aspx">Product</asp:HyperLink>
            </h1>
        </td>
        <td>
            <asp:Button ID="btnHome2" runat="server" OnClick="btnHome2_Click" Text="Home" Width="94px" />
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <asp:Image ID="Image39" runat="server" Height="299px" ImageUrl="~/Images/100.PNG" Width="623px" />
        </td>
        <td>
            <h2>Step 1.
                <br />
            </h2>
            <p>
                <br />
                This is the Product List Page<br />
                <br />
                Select a Product</p>
            <p>
                Or create a new Product Record press (new)</p>
            <p>
            </p>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <asp:Image ID="Image40" runat="server" Height="299px" ImageUrl="~/Images/101.PNG" Width="623px" />
        </td>
        <td>
            <h2>Step 2.<br />
            </h2>
            <p>
                Select a Product</p>
            <p>
                <br />
                Delete a Product Record press (Delete)</p>
            <p>
                Or Edit a Product Record press (Edit)</p>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <asp:Image ID="Image41" runat="server" Height="299px" ImageUrl="~/Images/102.PNG" Width="623px" />
        </td>
        <td>
            <h2>Step 3.</h2>
            <p>
                <br />
                <br />
                You can edit this current Product<br />
                <br />
                Then Submit it press &quot;Submit&quot;</p>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <asp:Image ID="Image42" runat="server" Height="299px" ImageUrl="~/Images/103.PNG" Width="623px" />
        </td>
        <td>
            <h2>Step 4.
                <br />
            </h2>
            <p>
                <br />
                If you have press the &quot;New&quot; button.
                <br />
                <br />
                You will be able to create a new Product Record then Submit it</p>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <h1>
                <asp:HyperLink ID="hplSupplierListPage" runat="server" NavigateUrl="~/Supplier/SupplierList.aspx">Supplier</asp:HyperLink>
            </h1>
        </td>
        <td>
            <asp:Button ID="btnHome3" runat="server" OnClick="btnHome3_Click" Text="Home" Width="94px" />
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <asp:Image ID="Image16" runat="server" Height="299px" ImageUrl="~/Images/20.PNG" Width="623px" />
        </td>
        <td>
            <h2>Step 1.
                <br />
            </h2>
            <p>
                <br />
                This is the Supplier List Page.<br />
                <br />
                Select a Supplier</p>
            <p>
                Or create a new Supplier Record press (new)</p>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <asp:Image ID="Image17" runat="server" Height="299px" ImageUrl="~/Images/21.PNG" Width="623px" />
        </td>
        <td>
            <h2>Step 2.<br />
            </h2>
            <p>
                Select a Supplier</p>
            <p>
                <br />
                Delete a SupplierRecord press (Delete)</p>
            <p>
                Or Edit a Supplier Record press (Edit)</p>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <asp:Image ID="Image18" runat="server" Height="299px" ImageUrl="~/Images/22.PNG" Width="623px" />
        </td>
        <td>
            <h2>Step 3.</h2>
            <p>
                <br />
                <br />
                You can edit this current Supplier<br />
                <br />
                Then Submit it press &quot;Submit&quot;</p>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <asp:Image ID="Image19" runat="server" Height="299px" ImageUrl="~/Images/23.PNG" Width="623px" />
        </td>
        <td>
            <h2>Step 4.
                <br />
            </h2>
            <p>
                <br />
                If you have selected the New button.
                <br />
                <br />
                You will be able to create a new Supplier Record then Submit it</p>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <h1>
                <asp:HyperLink ID="hplSupplierPurchaseListPage" runat="server" NavigateUrl="~/Purchase/PurchaseList.aspx">Supplier Purchase</asp:HyperLink>
            </h1>
        </td>
        <td>
            <asp:Button ID="btnHome4" runat="server" OnClick="btnHome4_Click" Text="Home" Width="94px" />
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <asp:Image ID="Image20" runat="server" Height="299px" ImageUrl="~/Images/24.PNG" Width="623px" />
        </td>
        <td>
            <h2>Step 1.
                <br />
            </h2>
            <p>
                <br />
                This is the Purchase List Page.<br />
                <br />
            </p>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <asp:Image ID="Image21" runat="server" Height="299px" ImageUrl="~/Images/25.PNG" Width="623px" />
        </td>
        <td>
            <p>
                Select a Purchase</p>
            <p>
                Or create a new Purchase Record press (new)</p>
            <p>
                To Edit press (Edit)</p>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <asp:Image ID="Image22" runat="server" Height="299px" ImageUrl="~/Images/26.PNG" Width="623px" />
        </td>
        <td>
            <h2>Step 2.<br />
            </h2>
            <p>
                Viewing or Editing a Purchase</p>
            <p>
                Before Deleting the Purchase you must Delete all the Purchase Lines.</p>
            <p>
                Submit once all fields are filled</p>
            <p>
                Insert the RawIngredient you wish to Purchase
            </p>
            <p>
                Or calculate the line total&nbsp;
            </p>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <asp:Image ID="Image23" runat="server" Height="299px" ImageUrl="~/Images/27.PNG" Width="623px" />
        </td>
        <td>
            <h2>Step 3. </h2>
            <p>
                <br />
                <br />
                If creating a new Purchase Record<br />
                <br />
                Fill the fields that arent grey</p>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <asp:Image ID="Image24" runat="server" Height="299px" ImageUrl="~/Images/28.PNG" Width="623px" />
        </td>
        <td>
            <h2>Step 4.
                <br />
            </h2>
            <p>
                <br />
                Once you have selected a Raw Ingredient and given the Quantity you want to buy
                <br />
                <br />
                Press (Calculate)</p>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <asp:Image ID="Image25" runat="server" Height="299px" ImageUrl="~/Images/29.PNG" Width="623px" />
        </td>
        <td>
            <h2>Step 5.<br />
            </h2>
            <p>
                <br />
                Press (Insert)</p>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <asp:Image ID="Image26" runat="server" Height="299px" ImageUrl="~/Images/30.PNG" Width="623px" />
        </td>
        <td>
            <h2>Step 6.
                <br />
            </h2>
            <p>
                <br />
                Now you can Submit the Purchase<br />
                <br />
                Delete the Purchase Line<br />
                <br />
                Then Delete the Purchase<br />
                <br />
                Or insert and calculate a new Raw Ingredient into the Purchase</p>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <asp:Image ID="Image27" runat="server" Height="299px" ImageUrl="~/Images/31.PNG" Width="623px" />
        </td>
        <td>
            <h2>Step 7.<br />
            </h2>
            <p>
                <br />
                After a sucessful Submit<br />
                <br />
                You will be redirected to the Purchase List Page</p>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <h1>
                <asp:HyperLink ID="hplRawIngredientListPage" runat="server" NavigateUrl="~/RawIngredient/RawIngredientList.aspx">Raw Ingredient</asp:HyperLink>
            </h1>
        </td>
        <td>
            <asp:Button ID="btnHome5" runat="server" OnClick="btnHome5_Click" Text="Home" Width="94px" />
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <asp:Image ID="Image28" runat="server" Height="299px" ImageUrl="~/Images/32.PNG" Width="623px" />
        </td>
        <td>
            <h2>Step 1.
                <br />
                <br />
            </h2>
            <p>
                Select a RawIngredient</p>
            <p>
                <br />
                Delete a RawIngredient Record press (Delete)</p>
            <p>
                Or Edit a RawIngredient press (Edit)</p>
            <p>
                Or create a new RawIngredient Record press (new)</p>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <asp:Image ID="Image29" runat="server" Height="299px" ImageUrl="~/Images/33.PNG" Width="623px" />
        </td>
        <td>
            <h2>Step 2.<br />
            </h2>
            <p>
                &nbsp;</p>
            You can edit this current Supplier<br />
            <br />
            Then Submit it<br />
            Press &quot;Submit&quot;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <asp:Image ID="Image30" runat="server" Height="299px" ImageUrl="~/Images/34.PNG" Width="623px" />
        </td>
        <td>
            <h2>Step 3.<br />
                <br />
            </h2>
            <p>
                <br />
                You can edit this current Supplier<br />
                <br />
                Then Submit it</p>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <asp:Image ID="Image31" runat="server" Height="299px" ImageUrl="~/Images/35.PNG" Width="623px" />
        </td>
        <td>After a successful save it will take you back to the RawIngredient List Page</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <h1>
                <asp:HyperLink ID="hplBranchListPage" runat="server" NavigateUrl="~/Branch/BranchList.aspx">Branch</asp:HyperLink>
            </h1>
        </td>
        <td>
            <asp:Button ID="btnHome6" runat="server" OnClick="btnHome6_Click" Text="Home" Width="94px" />
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <asp:Image ID="Image32" runat="server" Height="299px" ImageUrl="~/Images/36.PNG" Width="623px" />
        </td>
        <td>
            <h2>Step 1.
                <br />
                <br />
            </h2>
            <p>
                Select a Branch</p>
            <p>
                <br />
                Delete a Branch Record press (Delete)</p>
            <p>
                Or Edit a Branch press (Edit)</p>
            <p>
                Or create a new Branch Record press (new)</p>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <asp:Image ID="Image33" runat="server" Height="299px" ImageUrl="~/Images/38.PNG" Width="623px" />
        </td>
        <td>
            <h2>Step 2.<br />
            </h2>
            You can edit this current Branch
            <br />
            <br />
            Then Submit it<br />
            Press &quot;Submit&quot;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <asp:Image ID="Image34" runat="server" Height="299px" ImageUrl="~/Images/40.PNG" Width="623px" />
        </td>
        <td>After a successful save it will take you back to the Branch List Page</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <h1>
                <asp:HyperLink ID="hplPermissionListPage" runat="server" NavigateUrl="~/AdminHub/PermissionList.aspx">Admin Hub (Admin Only)</asp:HyperLink>
            </h1>
        </td>
        <td>
            <asp:Button ID="btnHome7" runat="server" OnClick="btnHome7_Click" Text="Home" Width="94px" />
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <asp:Image ID="Image35" runat="server" Height="299px" ImageUrl="~/Images/41.PNG" Width="623px" />
        </td>
        <td>
            <h2>Step 1.
                <br />
                <br />
            </h2>
            <p>
                Select a Employee Form</p>
            <p>
                <br />
                Delete a Employee Form Record press (Delete)</p>
            <p>
                Or Edit a Employee Form press (Edit)</p>
            <p>
                Or create a new Employee Form Record press (new)</p>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <asp:Image ID="Image36" runat="server" Height="299px" ImageUrl="~/Images/42.PNG" Width="623px" />
        </td>
        <td>
            <h2>Step 2.<br />
            </h2>
            You can edit this current Employee Form<br />
            <br />
            Then Submit it<br />
            Press &quot;Submit&quot;<br />
            <br />
            You will be redirected to the Permission Page</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <h1>Logout</h1>
        </td>
        <td>
            <asp:Button ID="btnHome8" runat="server" OnClick="btnHome8_Click" Text="Home" Width="94px" />
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <asp:Image ID="Image38" runat="server" Height="299px" ImageUrl="~/Images/44.PNG" Width="623px" />
        </td>
        <td>
            <p>
                To logout just press the logout button<br />
                <br />
                You will be logged out and redirected to the login page<br />
                <br />
                Press remeber to logout</p>
        </td>
    </tr>
</table>

