﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ChocoMamboOnlineProfessional.RawIngredient
{
    public partial class RawIngredient1 : System.Web.UI.UserControl
    {

        #region Variable Declaration

        RawIngredientClass _rawIngredient = null; // instance of the raw ingredient class
        long _PKID = 0; // variable used for the current primary key

        #endregion

        #region Accessor

        /// <summary>
        /// display the current record in all the related fields
        /// </summary>
        private void displayRecord()
        {
            txtIngredientName.Text = _rawIngredient.IngredientName;
            txtIngredientCode.Text = _rawIngredient.IngCode;
            txtPrice.Text = _rawIngredient.Price;
        }

        /// <summary>
        /// Pre-Condition: Method is called
        /// Post: determine what the access type is for the current user
        /// Description: checks current access right 
        /// </summary>
        /// <param name="pStrFormName"></param>
        /// <param name="phshTemp"></param>
        private void CheckAccessRights(string pStrFormName, Hashtable phshTemp)
        {
            if (phshTemp[pStrFormName].ToString() == "Deny")
            {
                Response.Redirect("/Home/Home.aspx");
            }
            if (phshTemp[pStrFormName].ToString() == "Read")
            {
                hideButton(btnSubmit);
            }
            if (phshTemp[pStrFormName].ToString() == "Write")
            {
                //do nothing here because nothing needs to be changed
            }
            if (phshTemp[pStrFormName].ToString() == "Admin")
            {
                //do nothing here because nothing needs to be changed
            }
        }

        /// <summary>
        /// Pre-Condition: User enters page 
        /// Post: Grabing the access right from a session variable then call the method and pass 
        /// the current page and current access right
        /// Description: checks the access rights
        /// </summary>
        private void CheckAccessRights()
        {
            Hashtable AccessRights = (Hashtable)Session["AccessRight"];
            if (AccessRights.Contains(("Ingredient")))
            {
                CheckAccessRights("Ingredient", AccessRights);
            }
            if (AccessRights.Contains(("AllPages")))
            {
                CheckAccessRights("AllPages", AccessRights);
            }
            else
            {
                Response.Redirect("/Home/Home.aspx");
            }
        }

        #endregion

        #region Mutator

        /// <summary>
        /// assign the related fields to the class
        /// </summary>
        private void AssignData()
        {
            _rawIngredient.IngredientName = txtIngredientName.Text;
            _rawIngredient.IngCode = txtIngredientCode.Text;
            _rawIngredient.Price = txtPrice.Text;
        }

        /// <summary>
        /// Pre-Conidition: Button isnt hidden
        /// Post-Conition: hides the button
        /// Description: When this method is called it gets passed a button to the paremeter 
        /// which is a new button object that buttons visibilty is set to false
        /// </summary>
        /// <param name="pbtnTemp"></param>
        private void hideButton(Button pbtnTemp)
        {
            pbtnTemp.Visible = false;
        }

        /// <summary>
        /// Clear the session variable 
        /// </summary>
        private void clearSession()
        {
            Session["RawIngredientPKID"] = "";
        }

        #endregion 

        #region Control Events

        protected void Page_Load(object sender, EventArgs e)
        {
            // if the session variable is empty pass the session variable to the global long varible
            if (Session["RawIngredientPKID"].ToString() != "" && long.TryParse(Session["RawIngredientPKID"].ToString(), out _PKID))
            {
                _PKID = long.Parse(Session["RawIngredientPKID"].ToString());
                // if the page isnt a post a back, grab the current record and display it
                if (!Page.IsPostBack)
                {
                    Session["Ingredient"] = _rawIngredient = new RawIngredientClass(_PKID);
                    displayRecord();
                }
                _rawIngredient = (RawIngredientClass)Session["Ingredient"]; // grab the current instance of the raw ingredient class if post back happen
            }
            else
            {
                // if the page isnt a post back, create a new record
                if (!Page.IsPostBack)
                {
                    Session["Ingredient"] = _rawIngredient = new RawIngredientClass();
                }
                _rawIngredient = (RawIngredientClass)Session["Ingredient"]; // grab the current instance of the raw ingredient class if post back happen
            }
            CheckAccessRights(); // check the access rights
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            AssignData(); // assign the data
            _rawIngredient.saveData(); // save the current record
            clearSession(); // clear the current session variable
            Response.Redirect("/RawIngredient/RawIngredientList.aspx"); // load the page
        }
        #endregion 

    }
}