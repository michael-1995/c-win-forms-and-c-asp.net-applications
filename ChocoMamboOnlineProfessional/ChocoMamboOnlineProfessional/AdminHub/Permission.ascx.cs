﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ChocoMamboOnlineProfessional.AdminHub
{
    public partial class Permission1 : System.Web.UI.UserControl
    {

        #region Variable Declaration

        PermissionClass _permission = null; // instance of the permission class
        long _PKID = 0; // variable used for the current primary key

        #endregion

        #region Accessor

        /// <summary>
        /// display the current record in all the related fields
        /// </summary>
        private void displayRecord()
        {
            cboEmployee.SelectedValue = _permission.EmployeeID.ToString();
            cboForm.SelectedValue = _permission.PageID.ToString();
            cboAccessTypes.Text = _permission.AccessType;
        }

        /// <summary>
        /// Pre-Condition: Method is called
        /// Post: determine what the access type is for the current user
        /// Description: checks current access right 
        /// </summary>
        /// <param name="pStrFormName"></param>
        /// <param name="phshTemp"></param>
        private void CheckAccessRights(string pStrFormName, Hashtable phshTemp)
        {
            if (phshTemp[pStrFormName].ToString() == "Deny")
            {
                Response.Redirect("/Home/Home.aspx");
            }
            if (phshTemp[pStrFormName].ToString() == "Read")
            {
                Response.Redirect("/Home/Home.aspx");
            }
            if (phshTemp[pStrFormName].ToString() == "Write")
            {
                Response.Redirect("/Home/Home.aspx");
            }
            if (phshTemp[pStrFormName].ToString() == "Admin")
            {
                //do nothing here because nothing needs to be changed
            }
        }

        /// <summary>
        /// Pre-Condition: User enters page 
        /// Post: Grabing the access right from a session variable then call the method and pass 
        /// the current page and current access right
        /// Description: checks the access rights
        /// </summary>
        private void CheckAccessRights()
        {
            Hashtable AccessRights = (Hashtable)Session["AccessRight"];
            if (AccessRights.Contains(("AdminHub")))
            {
                CheckAccessRights("AdminHub", AccessRights);
            }
            if (AccessRights.Contains(("AllPages")))
            {
                CheckAccessRights("AllPages", AccessRights);
            }
            else
            {
                Response.Redirect("/Home/Home.aspx");
            }
        }


        #endregion

        #region Mutator

        /// <summary>
        /// assign the related fields to the class
        /// </summary>
        private void assignData()
        {
            _permission.EmployeeID = long.Parse(cboEmployee.SelectedValue.ToString());
            _permission.PageID = long.Parse(cboForm.SelectedValue.ToString());
            _permission.Page = cboForm.SelectedItem.Text;
            _permission.Employee = cboEmployee.SelectedItem.Text;
            _permission.AccessType = cboAccessTypes.Text;
        }

        /// <summary>
        /// Pre-Condition: Method gets called
        /// Post-Condition: drop down is passed paremters values of the current drop downlist
        /// datatable, the data memeber and the value field. gives the datasource, data memeber 
        /// data value and then it binds the data
        /// Description: drop down list populates
        /// </summary>
        /// <param name="pdlist"></param>
        /// <param name="pDataTable"></param>
        /// <param name="pstrDataMember"></param>
        /// <param name="pstrDataValueField"></param>
        private void populateDropDownList(DropDownList pdlist, DataTable pDataTable, String pstrDataMember, String pstrDataValueField)
        {
            pdlist.DataSource = pDataTable;
            pdlist.DataTextField = pstrDataMember;
            pdlist.DataValueField = pstrDataValueField;
            pdlist.DataBind();
        }

        /// <summary>
        /// Pre-Condition: drop down list are emtpy
        /// Post-Condition: calls the method and passes paremeter values to populate the current 
        /// drop down list
        /// Description: populates multiple drop down list
        /// </summary>
        private void populateDropDownList()
        {
            populateDropDownList(cboEmployee, _permission.getEmployees(), "FirstName", "EmployeeID");
            populateDropDownList(cboForm, _permission.getForms(), "PageName", "PageID");
        }

        /// <summary>
        /// Clear the session variable 
        /// </summary>
        private void clearSession()
        {
            Session["EmployeeFormID"] = "";
        }

        #endregion 

        #region Control Events

        protected void Page_Load(object sender, EventArgs e)
        {
            // if the session variable is empty pass the session variable to the global long varible
            if (Session["EmployeeFormID"].ToString() != "" && long.TryParse(Session["EmployeeFormID"].ToString(), out _PKID))
            {
                _PKID = long.Parse(Session["EmployeeFormID"].ToString());
                // if the page isnt a post a back, grab the current record and display it
                if (!Page.IsPostBack)
                {
                    Session["Permission"] = _permission = new PermissionClass(_PKID);
                    displayRecord();
                    populateDropDownList();
                }
                _permission = (PermissionClass)Session["Permission"]; // grab the current instance of the permission class if post back happen
            }
            else
            {
                // if the page isnt a post back, create a new record
                if (!Page.IsPostBack)
                {
                    Session["Permission"] = _permission = new PermissionClass();
                    populateDropDownList();
                }
                _permission = (PermissionClass)Session["Permission"]; // grab the current instance of the permission class if post back happen
            }
            CheckAccessRights(); // check the access rights
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            assignData(); // assign the data
            _permission.saveData(); // save the current record
            clearSession(); // clear the current session variable
            Response.Redirect("/AdminHub/PermissionList.aspx"); // load the page
        }
        #endregion 

    }
}