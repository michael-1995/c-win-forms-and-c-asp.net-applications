﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ChocoMamboOnlineProfessional
{
    public partial class SiteMaster : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            NavigationMenu.Enabled = isLoggedIn();
            HeadLoginView.Visible = isLoggedIn();
        }
        /// <summary>
        /// Pre-Condition: Unknown if currently logged in
        /// Post-Condition: Check to see if you are on the login page to determine
        /// that you have not logged in yet
        /// Description: Determine if you are on the login page still then return either true or false
        /// depending if you are or not on the login page this method can be used to enable and disable buttons
        /// you wish for the user not to use when they have not logged in yet 
        /// </summary>
        /// <returns></returns>
        private Boolean isLoggedIn()
        {
            String path = HttpContext.Current.Request.Url.AbsolutePath;

            if (path == "/Login/UserLogin.aspx")
                return false;
            else
                return true;
        }
    }
}
