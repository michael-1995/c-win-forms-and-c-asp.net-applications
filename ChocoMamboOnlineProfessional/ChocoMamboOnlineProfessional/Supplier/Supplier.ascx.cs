﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ChocoMamboOnlineProfessional.Supplier
{
    public partial class Supplier1 : System.Web.UI.UserControl
    {

        #region Variable Declaration

        SupplierClass _supplier = null; // instance of the supplier class
        long _PKID = 0; // variable used for the current primary key

        #endregion

        #region Accessor

        /// <summary>
        /// display the current record in all the related fields
        /// </summary>
        private void displayRecord()
        {
            txtSupplierName.Text = _supplier.SupplierName;
            txtPhone.Text = _supplier.Phone;
            txtAddress.Text = _supplier.Address;
            txtPostCode.Text = _supplier.Postcode;
            txtSuburb.Text = _supplier.Suburb;
            txtState.Text = _supplier.State;
        }

        /// <summary>
        /// Pre-Condition: Method is called
        /// Post: determine what the access type is for the current user
        /// Description: checks current access right 
        /// </summary>
        /// <param name="pStrFormName"></param>
        /// <param name="phshTemp"></param>
        private void CheckAccessRights(string pStrFormName, Hashtable phshTemp)
        {
            if (phshTemp[pStrFormName].ToString() == "Deny")
            {
                Response.Redirect("/Home/Home.aspx");
            }
            if (phshTemp[pStrFormName].ToString() == "Read")
            {
                hideButton(btnSubmit);
            }
            if (phshTemp[pStrFormName].ToString() == "Write")
            {
                //do nothing here because nothing needs to be changed
            }
            if (phshTemp[pStrFormName].ToString() == "Admin")
            {
                //do nothing here because nothing needs to be changed
            }
        }

        /// <summary>
        /// Pre-Condition: User enters page 
        /// Post: Grabing the access right from a session variable then call the method and pass 
        /// the current page and current access right
        /// Description: checks the access rights
        /// </summary>
        private void CheckAccessRights()
        {
            Hashtable AccessRights = (Hashtable)Session["AccessRight"];
            if (AccessRights.Contains(("Supplier")))
            {
                CheckAccessRights("Supplier", AccessRights);
            }
            if (AccessRights.Contains(("AllPages")))
            {
                CheckAccessRights("AllPages", AccessRights);
            }
            else
            {
                Response.Redirect("/Home/Home.aspx");
            }
        }

        #endregion

        #region Mutator

        /// <summary>
        /// assign the related fields to the class
        /// </summary>
        private void AssignData()
        {
            _supplier.SupplierName = txtSupplierName.Text;
            _supplier.Phone = txtPhone.Text;
            _supplier.Address = txtAddress.Text;
            _supplier.Postcode = txtPostCode.Text;
            _supplier.Suburb = txtSuburb.Text;
            _supplier.State = txtState.Text;
        }

        /// <summary>
        /// Pre-Conidition: Button isnt hidden
        /// Post-Conition: hides the button
        /// Description: When this method is called it gets passed a button to the paremeter 
        /// which is a new button object that buttons visibilty is set to false
        /// </summary>
        /// <param name="pbtnTemp"></param>
        private void hideButton(Button pbtnTemp)
        {
            pbtnTemp.Visible = false;
        }

        /// <summary>
        /// Clear the session variable 
        /// </summary>
        private void clearSession()
        {
            Session["SuplierPKID"] = "";
        }


        #endregion 

        #region Control Events

        protected void Page_Load(object sender, EventArgs e)
        {
            // if the session variable is empty pass the session variable to the global long varible
            if (Session["SuplierPKID"].ToString() != "" && long.TryParse(Session["SuplierPKID"].ToString(), out _PKID))
            {
                _PKID = long.Parse(Session["SuplierPKID"].ToString());
                if (!Page.IsPostBack)
                {
                    Session["Supplier"] = _supplier = new SupplierClass(_PKID);
                    displayRecord();
                }
                _supplier = (SupplierClass)Session["Supplier"]; // grab the current instance of the supplier class if post back happen
            }
            else
            {
                if (!Page.IsPostBack)
                {
                    Session["Supplier"] = _supplier = new SupplierClass();
                }
                _supplier = (SupplierClass)Session["Supplier"]; // grab the current instance of the supplier class if post back happen
            }
            CheckAccessRights(); // check the access rights
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            AssignData(); // assign the data
            _supplier.saveData(); // save the current record
            clearSession(); // clear the current session variable
            Response.Redirect("/Supplier/SupplierList.aspx"); // load the page
        }
        #endregion 

    }
}